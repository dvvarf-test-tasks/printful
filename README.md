# General description

Since I'm not very familiar with Vue.js, the app is based on React framework and created using "Create React App".

Styling solution is umodified stock Bootstrap.

The only extra non-dev dependency is React Router to easier manage browser's history and navigate between routes in SPA setting.

Although Typescript was not used in this app, there are very simple JSDoc annotations in key places to help with reviewing the code in IDE.

There are no tests implemented as part of this task.

# Architecture

The app is a simple React Single Page Application. Backend is custom quiz API provided by Printful.

## Data types

Key data types:
* User
* Quiz
* Question
* Answer

Quiz data is a structure providing all known info about currently running quiz:
* ID
* title
* current state of the quiz (not started yet / started / finished)
* current position in the quiz (question index)
* list of questions (`Question`) with answers (`Answer`)

![Better than a thousand words](data-structure.png)*Basic data structues of the app*

Other temporary data (i.e. list of possible answers for current question) is stored inside component states.

## Components

The component names are closely related to data types (i.e. Quiz component for Quiz data type) and fetches necessary data automatically when mounted.

## State

Main application state is stored in single React context (`SessionContext`) accessible by all components of the app using hooks. This state consists of two main parts:
* user data (`User`)
* quiz data (`Quiz`)

App's whole lifecycle is managed through `SessionContext`, it provides the following methods to modify stored data:
* `setUserName` (sets user name; "logs user in")
* `initQuiz` (initializes quiz data with basic info: ID and title)
* `startQuiz` (allows fetching the questions)
* `finishQuiz` (allows checking the answers)
* `submitCurrentQuestionAnswer` (saves given answer to question at current position)
* `goToNextQuestion` (increments current quiz position)

## Considerations

Assumptions made about this app:
* It could be a part of a bigger app with separate authentication system, because of that the information about current user (name) is stored separately from state related to quizzes.
* There might be a possibility for user to has several quizzes in different states. Right now `SessionContext` only has a single `quiz` property in the maening of "current quiz", but a list of active/finished quizzes.
* Since moving through questions is restricted, quiz id and other information is not passed through browser's location and is only stored in application state.
* Storing application state in the browser (localStorage/sessionStorage) was not implemented, because it's not clear if it's necessary. It is simple to add, if necessary, since state is managed in one place.

## Typical lifecycle

1. *Home* component is rendered by default.
1. List of available quizzes is fetched and stored in component state.
1. User enters their name and selects the quiz, they are transfered to *Quiz* component. Basic user and quiz data set using `setUserName` and `initQuiz` methods.
1. *Quiz* component is rendered. If current quiz is not marked as "started", `startQuiz` is called to start fetching questions and store them in `SessionContext` (fetching is also performed in `SessionContext`).
1. Questions are fetched, *Question* component is rendered with current question (a given quiz position, default to "0").
1. User selects an answer and clicks "Next". Answer is saved in global state in using `submitCurrentQuestionAnswer` method. Then quiz position is incremented using `goToNextQuestion`. *Question* component is rendered with next questions.
1. On the last question, "Next" click triggers a call to `finishQuiz` and user is transferred to *Results* component.
1. *Results* is rendered, some checks are performed on answers given and then they are submitted to API.

## Available scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.
