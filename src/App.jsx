import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';

import { SessionProvider } from './contexts/SessionContext';

import ErrorBoundary from './components/ErrorBoundary';
import Home from './components/Home';
import Quiz from './components/Quiz';
import Results from './components/Results';

function App() {
  return (
    <ErrorBoundary>
      <BrowserRouter>
        <SessionProvider>
          <Routes>
            <Route exact path='/' element={<Home />} />
            <Route exact path='/quiz' element={<Quiz />} />
            <Route exact path='/results' element={<Results />} />
            {/* <Route exact path='/quiz/:quizId/question/:questionId' element={<Question />} /> */}
            <Route path='*' element={<Navigate to='/' />} />
          </Routes>
        </SessionProvider>
      </BrowserRouter>
    </ErrorBoundary>
  );
}

export default App;
