import { useContext } from 'react';

import { SessionContext } from '../contexts/SessionContext';

const useQuiz = () => {
  const context = useContext(SessionContext);

  const returnValue = context.quiz || {};

  return returnValue;
};

export default useQuiz;