import { useState, useEffect, useRef, useMemo } from 'react';

const useFetch = (url) => {
  const cache = useRef({});

  /** @type {{ loading: boolean; error: string; data: any }} */
  const initialState = useMemo(() => ({
    loading: false,
    error: undefined,
    data: undefined,
  }), []);

  const [response, setResponse] = useState(initialState);

  useEffect(() => {
    let unmounted = false;
    if (!url) return;

    const fetchData = async () => {
      setResponse({ ...initialState, loading: true });

      if (cache.current[url]) {
        const data = cache.current[url];
        setResponse({ ...initialState, loading: false, data });
      } else {
        try {
          const response = await fetch(url, {
            method: 'GET',
            redirect: 'follow',
          });

          const data = await response.json();

          if (data && typeof data['error'] === 'string') throw new Error(data.error);

          cache.current[url] = data;

          if (unmounted) return;

          setResponse({ ...initialState, loading: false, data });
        } catch (error) {
          if (unmounted) return;

          setResponse({ ...initialState, error: String(error) });
        }
      }
    };

    fetchData();

    return function cleanup() {
      unmounted = true;
    };
  }, [url, initialState]);

  return response;
};

export default useFetch;