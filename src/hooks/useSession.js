import { useContext } from 'react';

import { SessionContext } from '../contexts/SessionContext';

const useSession = () => {
  const context = useContext(SessionContext);

  return context;
};

export default useSession;