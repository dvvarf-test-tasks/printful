import React, { createContext, useEffect, useState } from 'react';

import useFetch from '../hooks/useFetch';

/** @typedef {() => void} VoidHandler */
/** @typedef {'init' | 'started' | 'finished'} QuizState */
/** @typedef {{ id: number, title: string }} Answer */
/** @typedef {{ id: number, title: string, answer?: Answer }} Question */
/** @typedef {{ name: string }} User */
/** @typedef {{ id: number, title: string, state: QuizState, questionIndex: number, questions?: Question[], questionsLoading: boolean, questionsError: string }} Quiz */
/** @typedef {{ user?: User, quiz?: Quiz, setUserName: (name: string) => void, initQuiz: (quiz: Quiz) => void, startQuiz: VoidHandler, finishQuiz: VoidHandler, submitCurrentQuestionAnswer: (answer: Answer) => void, goToNextQuestion: VoidHandler }} Session */

/**
 * @type {import('react').Context<Session>}
 */
const SessionContext = createContext();

function SessionProvider({ children }) {
  /** @type {[User, React.Dispatch<User>]} */
  const [user, setUser] = useState(undefined);

  /** @type {[Quiz, React.Dispatch<Quiz | import('react').SetStateAction<Quiz>>]} */
  const [quiz, setQuiz] = useState(undefined);

  const quizStarted = quiz && quiz.state === 'started';

  const { loading: questionsLoading, error: questionsError, data: questions } = useFetch(quizStarted ? `https://printful.com/test-quiz.php?action=questions&quizId=${quiz.id}` : undefined);

  useEffect(() => {
    const questionsWithAnswers = questions?.map(question => question.answer ? question : { ...question, answer: undefined });

    const questionsTotal = questions?.length;

    setQuiz(prevQuiz => ({ ...prevQuiz, questionsLoading, questionsError, questions: questionsWithAnswers, questionsTotal }));
  }, [questionsLoading, questionsError, questions]);

  /** @type {(name: string) => void} */
  const setUserName = (name) => {
    console.log(`User ${name} logged in.`);

    setUser(prevUser => ({ ...prevUser, name }));
  };

  /** @type {(quiz: Quiz) => void} */
  const initQuiz = (newQuiz) => {
    console.log(`Current quiz will be "${newQuiz.title}" (${newQuiz.id}).`);

    setQuiz({ ...newQuiz, state: 'init', questionIndex: 0 });
  };

  const startQuiz = () => {
    setQuiz(prevQuiz => ({ ...prevQuiz, state: 'started' }));
  };

  const finishQuiz = () => {
    if (!quiz) return;
    if (quiz.state !== 'started') return;

    console.log(`Quiz ${quiz.id} finished.`);

    setQuiz(prevQuiz => ({ ...prevQuiz, state: 'finished' }));
  };

  /** @type {(answer: Answer) => void} */
  const submitCurrentQuestionAnswer = (answer) => {
    console.log(`Answer "${answer.title}" (id=${answer.id}) was given to question "${quiz?.questions?.find((q, index) => (index === quiz.questionIndex))?.title}"`);

    setQuiz(prevQuiz => {
      if (!prevQuiz) return prevQuiz;

      const { questions: prevQuestions, ...restQuiz } = prevQuiz;

      return { ...restQuiz, questions: prevQuestions?.map((question, index) => (index === prevQuiz.questionIndex) ? { ...question, answer } : question) }
    });
  };

  const goToNextQuestion = () => {
    setQuiz(prevQuiz => {
      if (!prevQuiz) return prevQuiz;

      const { questionIndex: prevQuestionIndex, ...restQuiz } = prevQuiz;

      return { ...restQuiz, questionIndex: (prevQuestionIndex + 1) };
    });
  };

  const providerValue = {
    user,
    quiz,
    setUserName,
    initQuiz,
    startQuiz,
    finishQuiz,
    submitCurrentQuestionAnswer,
    goToNextQuestion,
  };

  return <SessionContext.Provider value={providerValue}>{children}</SessionContext.Provider>;
}

export {
  SessionContext,
  SessionProvider,
};