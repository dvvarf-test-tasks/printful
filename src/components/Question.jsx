import { useState } from 'react';

import useFetch from '../hooks/useFetch';

import CenterColumnBox from './CenterColumnBox';
import Loading from './Loading';
import Error from './Error';

function Question({ quizId, question, progress, onNext }) {
  const [selectedAnswer, setSelectedAnswer] = useState(undefined);

  const { loading, error, data: possibleAnswers = [] } = useFetch(`https://printful.com/test-quiz.php?action=answers&quizId=${quizId}&questionId=${question.id}`);

  if (loading) return <Loading>Loading answers...</Loading>

  if (error) return <Error>Error while loading answers: {error}</Error>

  const handleNextClick = () => {
    onNext && onNext(selectedAnswer);
  };

  return (
    <CenterColumnBox className='text-center'>
      <h2 className='mb-4'>{question.title}</h2>

      <div className='my-3'>
        <div className='container-fluid g-3'>
          <div className='row row-cols-1 row-cols-lg-2 g-3'>
            {
              possibleAnswers.map(
                possibleAnswer =>
                  <div
                    className='col'
                    key={possibleAnswer.id}
                  >
                    <div
                      className={`h-100 p-3 bg-light border ${(selectedAnswer?.id === possibleAnswer.id) ? 'border-primary text-primary' : ''}`}
                      onClick={() => setSelectedAnswer(possibleAnswer)}
                    >
                      {possibleAnswer.title}
                    </div>
                  </div>
                )
            }
          </div>
        </div>
      </div>

      <div className='m-3'>
        <div className='progress'>
          <div
            className='progress-bar progress-bar-striped'
            role='progressbar'
            style={{ width: `${progress}%` }}
            aria-valuenow={progress}
            aria-valuemin='0'
            aria-valuemax='100'
          ></div>
        </div>
      </div>

      <div className='m-3'>
        <input
          className='btn btn-outline-primary'
          type='button'
          value='Next'
          disabled={!selectedAnswer}
          onClick={handleNextClick}
        />
      </div>
    </CenterColumnBox>
  );
}

export default Question;