import { useState, useEffect, useRef } from 'react';
import { Toast as ToastBootstrap } from 'bootstrap';

function Toast({ children, autohide = true, animation = true, delay = 10000, onHidden }) {
  const [toastDisplayed, setToastDisplayed] = useState(true);
  const toastRef = useRef();

  useEffect(() => {
    if (!toastRef || !toastRef.current) return;

    const toastElement = toastRef.current;

    function hidden() {
      setToastDisplayed(false);
      onHidden && onHidden();
    }

    toastElement.addEventListener('hidden.bs.toast', hidden, false);
    return function cleanup() {
      toastElement.removeEventListener('hidden.bs.toast', hidden, false);
    };
  }, [onHidden]);

  useEffect(() => {
    const toastElement = toastRef.current;

    let bsToast = ToastBootstrap.getInstance(toastElement);

    if (!bsToast) {
      bsToast = new ToastBootstrap(toastElement, { autohide, animation, delay });
    }

    if (toastDisplayed) {
      bsToast.show();
    } else {
      bsToast.hide();
    }
  }, [animation, autohide, delay, toastDisplayed]);

  return (
    <div className='toast-container position-absolute p-3 bottom-0 start-50 translate-middle-x'>
      <div className='toast align-items-center alert-danger' role='alert' aria-live='assertive' aria-atomic='true' ref={toastRef}>
        <div className='d-flex'>
          <div className='toast-body'>
            {children}
          </div>
          <button type='button' className='btn-close me-2 m-auto' onClick={() => setToastDisplayed(false)} aria-label='Close'></button>
        </div>
      </div>
    </div>
  )
}

export default Toast;