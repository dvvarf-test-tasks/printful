function CenterColumnBox({ className = '', children }) {
  return (
    <div className='container' style={{ height: '100%', display: 'flex', flexDirection: 'column', alignContent: 'center', justifyContent: 'center' }}>
      <div className='row justify-content-md-center'>
        <div className={`col col-lg-6 ${className}`}>
          {children}
        </div>
      </div>
    </div>
  );
};

export default CenterColumnBox;