import CenterColumnBox from './CenterColumnBox';

function Error({ children }) {
  return (
    <CenterColumnBox className='text-center'>
      <div className='alert alert-danger' role='alert'>
        <h3 className='alert-heading'>Error</h3>
        {children}
      </div>
    </CenterColumnBox>
  );
}

export default Error;