import CenterColumnBox from './CenterColumnBox';
import Spinner from './Spinner';

function Loading({ children }) {
  return (
    <CenterColumnBox className='text-center'>
      <Spinner />
      <div>{ children }</div>
    </CenterColumnBox>
  );
}

export default Loading;