import { Component } from 'react';

import Error from './Error';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { error: undefined, errorInfo: undefined };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo
    });
  }

  render() {
    if (this.state.error) {
      return <Error>Unexpected error: {this.state.error && this.state.error.toString()}</Error>;
    }

    return this.props.children; 
  }
}

export default ErrorBoundary;