import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import useSession from '../hooks/useSession';
import useFetch from '../hooks/useFetch';

import CenterColumnBox from './CenterColumnBox';
import Loading from './Loading';
import Error from './Error';
import Toast from './Toast';

function Home() {
  const navigate = useNavigate();
  const { setUserName, initQuiz } = useSession();

  const { loading, error, data: quizzes } = useFetch('https://printful.com/test-quiz.php?action=quizzes');
  const [name, setName] = useState('');
  const [quizId, setQuizId] = useState('');
  const [validationError, setValidationError] = useState(undefined);

  const isQuizzesExist = quizzes && Array.isArray(quizzes) && quizzes.length > 0;

  /** @type {import('react').FormEventHandler<HTMLFormElement>} */
  const handleSubmit = (ev) => {
    ev.preventDefault();

    const isDataValid = (name !== '') && (quizId !== '');

    if (!isDataValid) {
      setValidationError('Name and quiz are both required!');
      return;
    }

    setUserName(name);

    const selectedQuiz = quizzes.find(quiz => quiz.id === quizId);
    initQuiz(selectedQuiz);

    navigate('/quiz');
  };

  if (loading) return <Loading>Loading test list...</Loading>

  if (error) return <Error>Error while test list: {error}</Error>

  return (
    <CenterColumnBox className='text-center'>
      <h2 className='mb-4'>Technical Task</h2>

      <form onSubmit={handleSubmit}>
        <div className='m-3'>
          <input
            className='form-control'
            type='text'
            value={name}
            placeholder='Enter your name...'
            onChange={(ev) => setName(ev.target.value)}
          />
        </div>

        <div className='m-3'>
          {
            isQuizzesExist ?
            <select className='form-select' onChange={(ev) => setQuizId(parseInt(ev.target.value, 10))} value={quizId}>
              <>
                <option value='' disabled>Choose test...</option>
                {
                  quizzes.map(quiz => <option key={quiz.id} value={quiz.id}>{quiz.title}</option>)
                }
              </>
            </select>
            :
            <select className='form-select' disabled>
              <option value='' disabled>No quizzes available</option>
            </select>
          }
        </div>

        <div className='m-3'>
          <input className='btn btn-outline-primary' type='submit' value='Start' />
        </div>
      </form>

      {
        validationError &&
          <Toast onHidden={() => setValidationError(undefined)}>{validationError}</Toast>
      }
    </CenterColumnBox>
  );
}

export default Home;