import { useEffect, useMemo } from 'react';
import { useNavigate, Link } from 'react-router-dom';

import useSession from '../hooks/useSession';
import useQuiz from '../hooks/useQuiz';

import Loading from './Loading';
import Error from './Error';
import Question from './Question';

function Quiz() {
  const navigate = useNavigate();

  const { startQuiz, finishQuiz, submitCurrentQuestionAnswer, goToNextQuestion } = useSession();
  const { id, state, questionsLoading, questionsError, questions, questionIndex, questionsTotal } = useQuiz();

  useEffect(() => {
    if (state === 'init') {
      startQuiz();
    }
  }, [startQuiz, state]);

  const currentQuestion = useMemo(() => (questions?.length > questionIndex ? questions[questionIndex] : undefined), [questionIndex, questions]);

  const progress = useMemo(() => (questionIndex/questionsTotal)*100, [questionIndex, questionsTotal]);

  const quizId = state === 'started' ? id : undefined;

  if (!quizId) return <Error><p>{ state === 'finish' ? 'Already finished the quiz' : 'Quiz not started yet' }</p> <Link to='/'>Return to home</Link></Error>;

  if (questionsLoading) return <Loading>Loading questions...</Loading>

  if (questionsError) return <Error>{`Error while loading questions: ${questionsError}`}</Error>

  if (!currentQuestion) return <Error><p>Unexpected error</p> <Link to='/'>Return to home</Link></Error>;

  const handleNextClick = (selectedAnswer) => {
    try {
      submitCurrentQuestionAnswer(selectedAnswer);
    }
    catch (error) { }

    const isLastQuestion = questionIndex >= (questionsTotal - 1);

    if (isLastQuestion) {
      finishQuiz();
      navigate('/results');
    } else {
      goToNextQuestion();
    }
  }

  return (
    <Question
      quizId={quizId}
      question={currentQuestion}
      progress={progress}
      onNext={handleNextClick}
    />
  );
}

export default Quiz;