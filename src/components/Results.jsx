import { Link } from 'react-router-dom';

import useSession from '../hooks/useSession';
import useFetch from '../hooks/useFetch';
import useQuiz from '../hooks/useQuiz';

import CenterColumnBox from './CenterColumnBox';
import Spinner from './Spinner';
import Error from './Error';

function Results() {
  const { user } = useSession();
  const { id, state, questions } = useQuiz();

  const quizId = state === 'finished' ? id : undefined;

  const answersReady = questions?.every(question => !!question.answer);

  const answerQuery = questions?.map(question => `answers[]=${question.answer?.id}`).join('&');

  const { loading, error, data: results = {} } = useFetch(quizId && answersReady ? `https://printful.com/test-quiz.php?action=submit&quizId=${quizId}&${answerQuery}` : undefined);

  if (state === 'started' && id && answersReady) return <Error>Quiz is not finished, but all answers are answered.</Error>;

  if (!quizId) return <Error>Quiz not finished! <Link to='/'>Return to home</Link></Error>

  if (!answersReady) return <Error>Not all answers are given! <Link to='/'>Return to home</Link></Error>;

  if (error) return <Error>Error while loading results: {error}</Error>

  return (
    <CenterColumnBox className='text-center'>
        <h2 className='mb-4'>Thanks, {user.name}!</h2>

        <div className='m-3'>
          <div>
            {
              loading ?
                <>
                  <Spinner />
                  <div>Loading results...</div>
                </>
              :
                <p>{`You responded correctly to ${results.correct} out of ${results.total} questions.`}</p>
            }
          </div>
        </div>

        <div>
          <Link to='/'>Return to home</Link>
        </div>
    </CenterColumnBox>
  );
}

export default Results;